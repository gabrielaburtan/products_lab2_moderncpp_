#include "Product.h"

Product::Product(const uint32_t& id, const std::string& name, const float& rawPrice) : m_id(id), m_name(name), m_rawPrice(rawPrice)
{
	// empty
}

uint32_t Product::GetID() const
{
	return this->m_id;
}

std::string Product::GetName() const
{
	return this->m_name;
}

float Product::GetRawPrice() const
{
	return this->m_rawPrice;
}
